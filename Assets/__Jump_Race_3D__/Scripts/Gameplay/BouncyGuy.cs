﻿using System;
using UnityEngine;

public class BouncyGuy : MonoBehaviour
{
    [SerializeField] Rigidbody _rb = null;
    [SerializeField] float moveSpeed = 5f;
    [SerializeField] float moveLerpSpeed = 5f;
    [SerializeField] float fallAnimLerpSpeed = 2f;

    [SerializeField] float rotationSpeed = 5f;

    [SerializeField] float minYvelocity = -45f;

    float _moveSpeed;
    float _rotationSpeed;

    bool isActive;

    [SerializeField] Animator _anim = null;

    public int LastPlatformID
    {
        get;
        private set;
    }

    private void Awake()
    {
        LastPlatformID = int.MaxValue;
        isActive = true;
    }

    private void FixedUpdate()
    {
        if (isActive)
        {
            Movement();
            Rotation();
        }
        VelocityCheck();
    }

    private void VelocityCheck()
    {
        float yVeloc = _rb.velocity.y < minYvelocity ? minYvelocity : _rb.velocity.y;

        _rb.velocity = new Vector3(0f, yVeloc, 0f);
    }

    private void Rotation()
    {
        float targetSpeed = InputController.Instance.HasSwipe ? rotationSpeed : 0f;

        if (InputController.Instance.LeftSwipe) targetSpeed = -targetSpeed;

        transform.Rotate(Vector3.up * targetSpeed * Time.deltaTime);
    }

    private void Movement()
    {
        float targetSpeed = 0f;
        float animTarget = 0f;

        if (InputController.Instance.HasTouch)
        {
            targetSpeed = moveSpeed;
            animTarget = 1f;
        }

        _moveSpeed = Mathf.Lerp(_moveSpeed, targetSpeed, moveLerpSpeed * Time.deltaTime);

        _anim.SetFloat(AnimStrings.ForwardFall, Mathf.Lerp(_anim.GetFloat(AnimStrings.ForwardFall), animTarget, fallAnimLerpSpeed * Time.deltaTime));

        transform.Translate(Vector3.forward * _moveSpeed);
    }

    private void OnCollisionEnter(Collision collision)
    {
        var plat = collision.gameObject.GetComponent<Platform>();
        if (plat)
        {
            var vel = _rb.velocity;
            vel.y = plat.NewVelocityY;
            _rb.velocity = vel;

            if (LastPlatformID - plat.ID > 1) PopUpText.Instance.DisplayLongJump();

            if (LastPlatformID > plat.ID) LastPlatformID = plat.ID;
            _anim.SetTrigger(AnimStrings.BackFlip);
            return;
        }

        if (collision.gameObject.tag == Tags.Finish)
        {
            _anim.SetTrigger(AnimStrings.Win);
            isActive = false;
        }

        if (collision.gameObject.tag == Tags.Sea)
        {
            isActive = false;
        }
    }

    private void Reset()
    {
        _rb = GetComponent<Rigidbody>();
    }

    private static class AnimStrings
    {
        public const string BackFlip = "Backflip";
        public const string FrontFlip = "Fackflip";
        public const string ForwardFall = "ForwardFall";
        public const string Win = "Win";
    }
}
