﻿using UnityEngine;

public class InputController : SingletonInstance<InputController>
{
    public bool HasTouch
    {
        get;
        private set;
    }
    public bool HasSwipe
    {
        get;
        private set;
    }
    public bool LeftSwipe
    {
        get;
        private set;
    }
    public bool RightSwipe
    {
        get;
        private set;
    }

    Vector2 prevPos;
    Vector2 curPos;

    [SerializeField] float swipeDeadzone = 2f;

    private void Update()
    {
        UpdateTouches();
        UpdateValues();
    }

    private void UpdateValues()
    {
        HasTouch = Input.touches.Length > 0 || Input.GetMouseButton(0);
        LeftSwipe = HasTouch && prevPos.x - curPos.x > swipeDeadzone;
        RightSwipe = HasTouch && curPos.x - prevPos.x > swipeDeadzone;
        HasSwipe = LeftSwipe || RightSwipe;
    }

    private void UpdateTouches()
    {
        prevPos = curPos;
        curPos = Input.mousePosition;

        if (Input.touches.Length > 0)
        {
            curPos = Input.touches[Input.touches.Length - 1].position;
        }
    }
}
