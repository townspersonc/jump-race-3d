﻿using UnityEngine;

public class LandingBeam : MonoBehaviour
{
    [SerializeField] Material correctMat = null;
    [SerializeField] Material incorrectMat = null;

    [SerializeField] Renderer _ren = null;

    [SerializeField] LayerMask targetLayer = 0;

    [SerializeField] float rayCastDistance = 1000f;

    private void Update()
    {
        bool hit = Physics.Raycast(transform.position, Vector3.down, out RaycastHit hitInfo, rayCastDistance, targetLayer);

        if(hit)
        {
            _ren.material = correctMat;
            //Division on 2 is because unity Cylinder is 2 units long.
            transform.localScale = new Vector3(transform.localScale.x, hitInfo.distance / 2f , transform.localScale.z);
        }
        else
        {
            _ren.material = incorrectMat;
            //Division on 2 is because unity Cylinder is 2 units long.
            transform.localScale = new Vector3(transform.localScale.x, rayCastDistance / 2f, transform.localScale.z);
        }
    }
}
