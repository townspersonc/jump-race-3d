﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalLoop : MonoBehaviour
{
    [SerializeField] float dist = 5f;
    [SerializeField] float time = 5f;

    Vector3 oneEnd, otherEnd;

    private void Start()
    {
        oneEnd = transform.position;
        otherEnd = transform.position;

        oneEnd.x -= dist;
        otherEnd.x += dist;

        transform.DOMove(oneEnd, time / 2f).SetEase(Ease.Linear).OnComplete(Loop);
    }

    void Loop()
    {
        Sequence s = DOTween.Sequence();

        s.Append(transform.DOMove(otherEnd, time).SetEase(Ease.Linear)).Append(transform.DOMove(oneEnd, time).SetEase(Ease.Linear)).SetLoops(int.MaxValue);
    }
}
