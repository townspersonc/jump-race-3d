﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathRenderer : MonoBehaviour
{
    [SerializeField] LineRenderer _lr = null;
    [SerializeField] PlatformSpawner platSpawn = null;

    private void Start()
    {
        DrawLine();
    }

    private void DrawLine()
    {
        List<Platform> plats = platSpawn.Platforms;

        _lr.positionCount = plats.Count;

        for (int i = 0; i < plats.Count; i++)
        {
            Vector3 linePos = plats[i].transform.position;
            linePos.y -= 2f;

            _lr.SetPosition(i, linePos);
        }
    }
}
