﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlatformSpawner : MonoBehaviour
{
    [SerializeField] List<Platform> platformPrefabs = null;
    [SerializeField] Platform seaPlatform = null;
    [SerializeField] Transform finish = null;

    [SerializeField] Vector3 platOffset = Vector3.zero;
    [SerializeField] float maxAngleOffset = 90f;

    List<Platform> platforms = new List<Platform>();
    public List<Platform> Platforms => platforms;
    List<Platform> seaPlatforms = new List<Platform>();

    public Transform LastPlatform => platforms.Last().transform;


    public int Amount => 15 + Save.Level * 5;

    [ContextMenu("Spawn")]
    public void Spawn()
    {
        Clear();
        PopulatePlatforms();
    }

    private void PopulatePlatforms()
    {
        Transform prev = finish;

        for (int i = 0; i < Amount; i++)
        {
            Platform p = Instantiate(platformPrefabs.Random(), prev.transform.position, prev.transform.rotation, transform).GetComponent<Platform>();

            p.transform.rotation *= Quaternion.AngleAxis(Random.Range(-maxAngleOffset, maxAngleOffset), Vector3.up);

            p.transform.Translate(platOffset);

            SpawnSeaPlatforms(p.transform.position);
            p.Init(i + 1);

            platforms.Add(p);
            prev = p.transform;
        }
    }

    [ContextMenu("Clear")]
    void Clear()
    {
        for (int i = platforms.Count - 1; i >= 0; i--)
        {
            if (Application.isPlaying)
            {
                Destroy(platforms[i].gameObject);
            }
            else
            {
                DestroyImmediate(platforms[i].gameObject);
            }
        }
        for (int i = seaPlatforms.Count - 1; i >= 0; i--)
        {
            if (Application.isPlaying)
            {
                Destroy(seaPlatforms[i].gameObject);
            }
            else
            {
                DestroyImmediate(seaPlatforms[i].gameObject);
            }
        }

        platforms.Clear();
        seaPlatforms.Clear();
    }

    void SpawnSeaPlatforms(Vector3 underPosition)
    {
        var plat = Instantiate(seaPlatform, new Vector3(Random.Range(underPosition.x - 25f, underPosition.x + 25f), 0f, Random.Range(underPosition.z - 25f, underPosition.z + 25f)), Quaternion.identity, transform); ;
        plat.Init(int.MaxValue);
        seaPlatforms.Add(plat);
    }
}
