﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : SingletonInstance<GameManager>
{
    [SerializeField] BouncyGuy player = null;
    [SerializeField] PlatformSpawner platformSpawner = null;

    Coroutine reloadRoutine;

    protected override void PersonalAwake()
    {
        platformSpawner.Spawn();
        PutPlayerOnLastPlatform();
    }

    private void PutPlayerOnLastPlatform()
    {
        Transform t = platformSpawner.LastPlatform;

        player.transform.position = new Vector3(t.position.x, t.position.y + 5f, t.position.z);
        player.transform.rotation = t.rotation * Quaternion.AngleAxis(-90f, Vector3.up);
    }

    public void LevelFinished()
    {
        Save.Level = Save.Level + 1;
        PopUpText.Instance.DisplayLevelComplete();
        ReloadSceneInTime();
    }

    public void LevelFailed()
    {
        ReloadSceneInTime(0.3f);
    }

    void ReloadSceneInTime(float time = 3f)
    {
        if (reloadRoutine != null) StopCoroutine(reloadRoutine);

        reloadRoutine = StartCoroutine(Reload(time));
    }

    private IEnumerator Reload(float time = 3f)
    {
        yield return new WaitForSeconds(time);

        reloadRoutine = null;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }


    private void Reset()
    {
        player = FindObjectOfType<BouncyGuy>();
        platformSpawner = FindObjectOfType<PlatformSpawner>();
    }
}
