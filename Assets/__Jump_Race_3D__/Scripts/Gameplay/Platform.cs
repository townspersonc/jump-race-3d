﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Platform : MonoBehaviour
{
    [SerializeField] GameObject particle = null;

    [SerializeField] float newVelocityY = 5f;
    [SerializeField] TMP_Text idText = null;
    public float NewVelocityY => newVelocityY;
    public int ID
    {
        get;
        private set;
    }

    bool firstLanding;

    [SerializeField] float perfectRange = 1f;
    [SerializeField] float goodRange = 2f;

    [SerializeField] Animator _anim = null;

    [SerializeField] List<Breakable> breakables = null;

    public void Init(int id)
    {
        ID = id;
        firstLanding = true;
        if(idText)idText.text = ID.ToString();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(firstLanding && collision.gameObject.tag == Tags.Player)
        {
            float dist = Vector2.Distance(new Vector2(transform.position.x, transform.position.z), new Vector2(collision.gameObject.transform.position.x, collision.gameObject.transform.position.z));

            if ( dist < perfectRange)
            {
                PopUpText.Instance.DisplayPerfect();
            }
            else if (dist < goodRange)
            {
                PopUpText.Instance.DisplayGood();
            }

            firstLanding = false;
        }


        if(_anim) _anim.SetTrigger(AnimStrings.Bounce);
        else if(breakables.Count > 0)
        {
            ActivateBreakables();
        }

        SpawnParticle();
    }

    void SpawnParticle()
    {
        Transform t = Instantiate(particle).transform;
        t.position = transform.position;
    }

    void ActivateBreakables()
    {
        foreach(Breakable i in breakables)
        {
            i.Break();
            i.transform.parent = null;
        }

        gameObject.SetActive(false);
    }

    private static class AnimStrings
    {
        public const string Bounce = "Bounce";
    }
}
