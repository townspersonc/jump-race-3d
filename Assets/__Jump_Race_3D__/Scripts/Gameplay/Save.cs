﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Save
{
    public static int Level
    {
        get
        {
            return PlayerPrefs.GetInt(SafeKeys.Level, 1);
        }
        set
        {
            PlayerPrefs.SetInt(SafeKeys.Level, value);
        }
    }

    private static class SafeKeys
    {
        public const string Level = "Level";
    }
}
