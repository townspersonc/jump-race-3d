﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Finish : MonoBehaviour
{
    [SerializeField] GameObject winParticle = null;

    [SerializeField] List<Transform> particleSpawnPoints = new List<Transform>();

    void FinishReachedByPlayer()
    {
        SpawnParticles();
        GameManager.Instance.LevelFinished();
    }

    void SpawnParticles()
    {
        foreach (Transform i in particleSpawnPoints)
        {
            Transform t = Instantiate(winParticle).transform;

            t.position = i.position;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == Tags.Player)
        {
            FinishReachedByPlayer();
        }
    }
}
