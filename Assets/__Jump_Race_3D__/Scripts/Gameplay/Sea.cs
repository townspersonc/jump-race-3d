﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sea : MonoBehaviour
{
    [SerializeField] GameObject splashParticle = null;
    private void PlayerEnteredTheSea()
    {
        GameManager.Instance.LevelFailed();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == Tags.Player)
        {
            PlayerEnteredTheSea();
        }

        SpawnParticle(collision.contacts[0].point);
    }

    void SpawnParticle(Vector3 pos)
    {
        Transform p = Instantiate(splashParticle).transform;
        p.position = new Vector3(pos.x, transform.position.y + 0.51f, pos.z);
    }
}
