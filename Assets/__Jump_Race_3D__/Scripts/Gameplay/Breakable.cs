﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Breakable : MonoBehaviour
{
    [SerializeField] Rigidbody _rb = null;

    [SerializeField] float destroyTime = 10f;

    public void Break()
    {
        _rb.isKinematic = false;

        StartCoroutine(DestroyRoutine(destroyTime));
    }

    IEnumerator DestroyRoutine(float time)
    {
        yield return new WaitForSeconds(time);

        Destroy(gameObject);
    }

    private void Reset()
    {
        _rb = GetComponent<Rigidbody>();
    }
}
