﻿using UnityEngine;

public class SingletonInstance<T> : MonoBehaviour where T : MonoBehaviour
{
    protected static T _instance;
    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject obj = GameObject.Find(typeof(T).Name);
                if (obj)
                {
                    _instance = obj.GetComponent<T>();
                }
                else
                {
                    obj = new GameObject();
                    obj.name = typeof(T).Name;
                    _instance = obj.AddComponent<T>();
                }
            }
            return _instance;
        }
    }

    protected virtual void Awake()
    {
        if (_instance == null)
        {
            _instance = this as T;
        }
        else
        {
            if (this != _instance)
            {
                Destroy(this.gameObject);
                return;
            }
        }

        PersonalAwake();
    }

    protected virtual void PersonalAwake()
    {
    }
}
