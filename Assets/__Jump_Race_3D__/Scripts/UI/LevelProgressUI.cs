﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelProgressUI : MonoBehaviour
{
    [SerializeField] Slider slider = null;

    [SerializeField] Text curLevel = null;
    [SerializeField] Text nextLevel = null;

    [SerializeField] BouncyGuy player = null;
    [SerializeField] PlatformSpawner platSpawn = null;

    private void Start()
    {
        SetLevelTexts();
    }

    private void Update()
    {
        slider.value = ((float)(platSpawn.Amount - player.LastPlatformID)) / ((float)platSpawn.Amount);
    }

    private void SetLevelTexts()
    {
        int l = Save.Level;

        curLevel.text = l.ToString();
        nextLevel.text = (l + 1).ToString();
    }
}
