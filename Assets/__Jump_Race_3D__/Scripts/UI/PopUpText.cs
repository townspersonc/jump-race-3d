﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;

public class PopUpText : SingletonInstance<PopUpText>
{
    [SerializeField] TMP_Text text = null;

    public void Display(string str, Color col, Ease ease = Ease.InSine)
    {
        DOTween.Kill(text.transform);

        text.color = col;
        text.text = str;
        Sequence s = DOTween.Sequence();
        s.Append(text.transform.DOScale(1.3f, 0.3f).SetEase(ease)).AppendInterval(0.5f).Append(text.transform.DOScale(0f, 0.3f).SetEase(ease));
    }

    public void DisplayGood()
    {
        Display("Good", new Color(0f, 0.8588235f, 0.8470588f));
    }

    public void DisplayPerfect()
    {
        Display("Perfect", new Color(0f, 0.8862745f, 0.01176471f));
    }

    public void DisplayLongJump()
    {
        Display("Long Jump", new Color(1f, 0.9882353f, 0f));
    }

    public void DisplayLevelComplete()
    {
        Display("Level Complete", new Color(0.9150943f, 0.1352497f, 0.1352497f));
    }
}
