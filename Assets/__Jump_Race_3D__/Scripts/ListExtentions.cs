﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ListExtentions
{
    /// <summary>
    /// returns id as if it where carouseled on numbers from 0 to list.count. For example: id = 7, list.count = 3, result is 1. id = -1, count = 2, result is 2.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    public static int CarouselID<T>(this List<T> list, int id)
    {
        return BonusMath.RealModulo(id, list.Count);
    }

    /// <summary>
    /// returns item at id as if id where carouseled on numbers from 0 to list.count. For example: id = 7, list.count = 3, result is 1. id = -1, count = 2, result is 2.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    public static T Carousele<T>(this List<T> list, int id)
    {
        return list[list.CarouselID(id)];
    }

    /// <summary>
    /// Returns member on imidiate right of ID, edges work like carousele.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    public static T Next<T>(this List<T> list, int id)
    {
        return list[list.CarouselID(id + 1)];
    }

    /// <summary>
    /// Returns member on imidiate left of ID, edges work like carousele.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    public static T Prev<T>(this List<T> list, int id)
    {
        return list[list.CarouselID(id - 1)];
    }

    /// <summary>
    /// Returns Random member of array.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    /// <returns></returns>
    public static T Random<T>(this List<T> list)
    {
        if (list.Count == 0)
        {
            return default(T);
        }

        return list[UnityEngine.Random.Range(0, list.Count)];
    }

    public static void Shuffle<T>(this List<T> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            T tmp = list[0];
            list.RemoveAt(0);
            list.Insert(UnityEngine.Random.Range(0, list.Count), tmp);
        }
    }
}
